
import * as pluginGatsbyNode0 from "../node_modules/gatsby-plugin-image/gatsby-node"
import * as pluginGatsbyNode1 from "../node_modules/gatsby-source-filesystem/gatsby-node"
import * as pluginGatsbyNode2 from "../node_modules/gatsby-transformer-sharp/gatsby-node"
import * as pluginGatsbyNode3 from "../node_modules/gatsby-plugin-sharp/gatsby-node"
import * as pluginGatsbyNode4 from "../node_modules/gatsby-plugin-page-creator/gatsby-node"
import * as pluginGatsbyWorker0 from "../node_modules/gatsby-plugin-sharp/gatsby-worker"

export const gatsbyNodes = {
"gatsby-plugin-image": pluginGatsbyNode0,
"gatsby-source-filesystem": pluginGatsbyNode1,
"gatsby-transformer-sharp": pluginGatsbyNode2,
"gatsby-plugin-sharp": pluginGatsbyNode3,
"gatsby-plugin-page-creator": pluginGatsbyNode4,
}

export const gatsbyWorkers = {
"gatsby-plugin-sharp": pluginGatsbyWorker0,
}

export const flattenedPlugins =
  [
  {
    "resolve": "",
    "id": "a9f9c4c1-d74b-569b-a6ec-6093f5d12592",
    "name": "gatsby-plugin-image",
    "version": "2.16.1",
    "pluginOptions": {
      "plugins": []
    },
    "nodeAPIs": [
      "createSchemaCustomization",
      "onCreateBabelConfig",
      "onCreateWebpackConfig",
      "preprocessSource"
    ],
    "browserAPIs": [],
    "ssrAPIs": [
      "onRenderBody"
    ],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "3a7b0c31-f5b8-59cd-9f50-8c4a64db496b",
    "name": "gatsby-source-filesystem",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "name": "images",
      "path": "/home/amisaka/my-static-website/src/images"
    },
    "nodeAPIs": [
      "onPreInit",
      "pluginOptionsSchema",
      "sourceNodes",
      "setFieldsOnGraphQLNodeType"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "822bdf7b-a95a-5885-9351-158705910ac3",
    "name": "gatsby-transformer-sharp",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": []
    },
    "nodeAPIs": [
      "onPreInit",
      "onCreateNode",
      "unstable_shouldOnCreateNode",
      "createSchemaCustomization",
      "createResolvers"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "6511e980-81d6-5cdd-af57-9e51db91e678",
    "name": "gatsby-plugin-sharp",
    "version": "4.16.1",
    "pluginOptions": {
      "plugins": [],
      "base64Width": 20,
      "stripMetadata": true,
      "defaultQuality": 50,
      "failOnError": true
    },
    "nodeAPIs": [
      "onCreateDevServer",
      "onPostBootstrap",
      "onPluginInit",
      "onPreBootstrap",
      "pluginOptionsSchema"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "6bd38c1e-1506-582f-b113-24f85ce25721",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby/dist/internal-plugins/dev-404-page/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "1648e9cf-7cdc-5fce-b4f1-c7dab9ce9447",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby/dist/internal-plugins/load-babel-config/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "c3103513-3793-5732-b6d3-044ba7ba7df8",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby/dist/internal-plugins/internal-data-bridge/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "67181ee7-172b-5b96-b4c8-5bef6854db3a",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby/dist/internal-plugins/prod-404-500/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "2792bbb3-3891-5458-a230-72308d432534",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby/dist/internal-plugins/webpack-theme-component-shadowing/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "af9a3723-89b5-5505-82a9-7010576fa418",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby/dist/internal-plugins/bundle-optimisations/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "52b4dd82-68cb-53a3-ab46-77a106288683",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby/dist/internal-plugins/functions/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "d951f16b-344f-5e35-b3dc-e825658a9d4a",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby-plugin-react-helmet/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "f2bafe1e-7164-50ac-abed-2b6c797e90ec",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby-plugin-image/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "feabc350-5081-5af0-ae69-50fa1e43731a",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby-source-filesystem/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "5afe39d0-27e4-5c66-915a-9bae27c400e8",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby-transformer-sharp/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "ef9b8f0a-754f-5b57-8dfc-b820d1a4f7ad",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby-plugin-sharp/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "555bb4fd-215c-5e14-86d4-38dfe89e41bd",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/node_modules/gatsby-plugin-manifest/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  },
  {
    "resolve": "",
    "id": "a6d57a81-9185-5e04-b084-ac6ae0ee9d94",
    "name": "gatsby-plugin-page-creator",
    "version": "4.16.0",
    "pluginOptions": {
      "plugins": [],
      "path": "/home/amisaka/my-static-website/src/pages",
      "pathCheck": false
    },
    "nodeAPIs": [
      "createPagesStatefully",
      "setFieldsOnGraphQLNodeType",
      "onPluginInit"
    ],
    "browserAPIs": [],
    "ssrAPIs": [],
    "pluginFilepath": ""
  }
]
